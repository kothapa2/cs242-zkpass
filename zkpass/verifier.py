"""
Verifier checks proofs of knowledge created by prover.

Verifier deals exclusively with cryptographic objects from the cryptography library such as Points, Fp and group elements. The server class serves a sa wrapper for verifier dealing only with string representations of these objects.

"""

__author__ = 'Abhiram Kothapalli'

from cryptography.elliptic import Point
from cryptography.secp256k1 import q, Fq, order, p, Fp, G, curve, ser, deser, uint256_from_str, uint256_to_str, make_random_point
from cryptography.utilities import *
import os, random



class Verifier():


    def verify(self, X, proof):

        # Unpack the proof which consists fo K and s
        (K,s) = proof

        # Type check both K and s
        assert type(X) is type(K) is Point
        assert type(s) is Fp

        # The verifier also computes the challenge. The trick is that this challenge
        # is the exact same is the challenge computed by the prover because sha2
        # is deterministic.
        c = uint256_from_str(sha2(ser(K)))

        # Provided that the prover has created a valid proof, the verifier simply
        # needs to check that g^s = X^challenge * K
        if s.n *G == K + c*X:
            return True
        else:
            return False
