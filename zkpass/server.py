"""
Server recieves requests to make accounts and processes authentication requests

Server serves as a wrapper around Verifier. While Verifier deals with objects from the cryptographic libraries, Server conveniently takes all inputs as strings and passes all outputs as strings.

"""

__author__ = 'Abhiram Kothapalli'

from verifier import Verifier
from utilities.bloomfilter import BloomFilter
from utilities.logger import Logger
from cryptography.secp256k1 import ser, deser
from cryptography.utilities import str_to_proof

class Server():

    logger = Logger()
    verifier = Verifier()
    bloom = BloomFilter(50000, 10)
    commitments = set([])

    # Can be expanded in future weeks.
    def __init__(self):
        return


    def register(self, commitment):
        """register handles a registration request from a client

        In particular register puts the commitment in its set of known commitments

        Args:
        commitment: A string representing a commitment

        """

        self.log("Received registration request " + str(commitment))

        # Store commitment in database
        self.commitments.add(commitment)

    def authenticate(self, commitment, proof):
        """authenticate handles an authentication request from a client

        In particular authenticate first checks that the commitment exists in the 
        database, then checks that proof has not been used before using a bloom filter, 
        then checks that the proof is valid.

        """

        self.log("Received authentication request " + str(commitment) + " " + str(proof))

        # Check that the commitment exists in the database
        if commitment not in self.commitments:
            self.log("Commitment does not exist in database")
            self.handle_authentication_failure(commitment)
            return False

        # Check that this proof has not been used before.
        # This prevents replay attacks.
        # In the worst case a valid proof is rejected, the client should simply retry
        if proof in self.bloom:
            self.log("Proof has potentially been used before")
            self.handle_authentication_failure(commitment)
            return False
        else:
            self.bloom.add(proof)
        
        
        # Verify the proof
        K, s = str_to_proof(proof)
        if self.verifier.verify(deser(commitment), (K, s)):
            self.handle_authentication_success(commitment)
            return True
        else:
            self.log("Invalid proof")
            self.handle_authentication_failure(commitment)
            return False

    # Handles failed authentication
    # Can be expanded in later weeks.
    def handle_authentication_failure(self, commitment):
        self.log("Authentication failure")
        return

    # Handles authentication success
    # Can be handled in later weeks.
    def handle_authentication_success(self, commitment):
        self.log("Authentication success")
        return

    # Allows server to log using the Logger class.
    def log(self, msg):
        self.logger.log("Server", msg)

    
