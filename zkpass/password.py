"""
Password generates a commitment and salt from provided password
"""


"""
Password contains helper functions to create and store an exponent created by a string password.

"""

__author__ = 'Abhiram Kothapalli'

from cryptography.secp256k1 import uint256_from_str, G, Fq, Point, curve, ser, deser
from cryptography.utilities import rnd_bytes, H
import os


class Password():

    def make_commitment(self, password):
        """make_commitment creates a commitment from a password.
        
        In particular make_commitment first converts a password to
        an integer using password_to_exponent. Then it returns
        a commitment as specified by the Schnorr protocol.

        """

        # Create integer from password.
        x = self.password_to_exponent(password)

        # Schnorr commitment simply requires using x as an exponent.
        return x * G
        
    def password_to_exponent(self, password):
        """password_to_exponent converts a password to an integer

        In particular password_to_exponent pads the password to 32 bytes
        then converts it to an int using helper functions in the cryptography
        library. Note that if the password is greater than 32 bytes, this
        function throws an error.

        """

        # Check that password is under 32 bytes.
        assert len(password) <= 32

        # First pad password so it is 32 bytes.
        padded_password = password.ljust(32, '0')

        # Now convert the padded password to an int.
        x = uint256_from_str(padded_password)

        return x

    # Potentially useful in future weeks
    def write_commitment_to_file(self, filename, C):
        open(filename, 'w').write(ser(C))

    # Potentially useful in future weeks
    def read_commitment_from_file(self, filename):
        return deser(open(filename, 'r').read())


