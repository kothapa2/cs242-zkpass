"""
Utility functions

Contains a list of functions that help utilize the cryptography library

"""

__author__ = 'Abhiram Kothapalli'

from elliptic import Point
from secp256k1 import q, Fq, order, p, Fp, G, curve, ser, deser, uint256_from_str, uint256_to_str, make_random_point
import os, random

Hx = Fq(0x8E3871A594F9AF7A1F357A0793124AAF3358B0F020983678BCD411EE6AF387A5L)
Hy = Fq(0x83BFAA8176272D0E4D7AD3577F3A0A3B70D7E1BFC2B638CA2807562F2CA85F59L)
H = Point(curve, Hx,Hy)

# Random oracle instantiated with SHA2 hash.
def sha2(x):
    from Crypto.Hash import SHA256
    return SHA256.new(x).digest()


# Random bytes currently sourced by os.urandom.
def rnd_bytes(x):
    return os.urandom(x)

# Converts a proof to a string.
def proof_to_str(proof):

    K, s = proof
    
    K, s = proof
    prefix = ser(K)
    
    return prefix + " " + str(s)

# Converts a string to a proof.
def str_to_proof(prf_str):
    prefix, s_str = prf_str.split(" ")
    K = deser(prefix)
    s = Fp(long(s_str))
    return K, s
