"""
Implementation of bloom filter

Most of the implementation details are borrowed from

http://www.maxburstein.com/blog/creating-a-simple-bloom-filter/

"""

__author__ = 'Abhiram Kothapalli'

from bitarray import bitarray
import mmh3

class BloomFilter():

    # Sets configurations of bloom filter
    def __init__(self, size, hash_count):
        self.size = size
        self.hash_count = hash_count
        self.bit_array = bitarray(size)
        self.bit_array.setall(0)

    # To be implemented in future weeks
    def read_filter_from_file(filename):
        pass

    # To be implemented in future weeks
    def write_filter_to_file(filename):
        pass

    # Adds a string to the filter
    def add(self, commitment):

        for seed in range(0, self.hash_count):
            index = mmh3.hash(commitment, seed) % self.size
            self.bit_array[index] = 1

    # Overrides the 'in' keyword to check if elements are in the bloom filter
    def __contains__(self, commitment):
        for seed in range(0, self.hash_count):
            index = mmh3.hash(commitment, seed) % self.size
            if self.bit_array[index] == 0:
                return False
            return True
        
