"""
Logger handles global logging

Logger is used by the Client and Server classes

"""

__author__ = 'Abhiram Kothapalli'

import logging

class Logger():

    def __init__(self):
        return

    # Can use the logging class in future weeks.
    def log(self, name, message):
        #logging.warning(name + ": " + message)
        print(name + ": " + message)
