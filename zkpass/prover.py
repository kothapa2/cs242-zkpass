"""
Prover generates a proof of knowledge of secret exponent

Prover deals exclusively with cryptographic objects from the cryptography library
such as Points, Fp, and group elements. The client class serves as a wrapper for 
prover dealing only with string representations of these objects.
"""

__author__ = 'Abhiram Kothapalli'

from cryptography.elliptic import Point
from cryptography.secp256k1 import q, Fq, order, p, Fp, G, curve, ser, deser, uint256_from_str, uint256_to_str, make_random_point
from cryptography.utilities import *
import os, random



class Prover():

    def prove(self, x):
        """prove creates a proof that the client contains x

        Specifically, the proof follows the steps of the Schnorr Identification 
        Protocol.
        
        Args:
        x: The provers secret, this is seeded from a password
        
        Returns:
        A proof that the prover knows x without revealing x

        """

        # Prover creates a blinding factor k so that it does not need
        # to reveal x in order to prove that it has x
        k = uint256_from_str(rnd_bytes(32)) % order
        
        # Create K which is used later in the protocol by the verifier.
        K = k*G

        # Prover creates a challenge, c, by itself by hashing K. While this has
        # the same security guarantees as the server sending a challenge,
        # this allows the protocol to be completed within one network
        # message instead of the standard 3 employed by SSH.
        c = uint256_from_str(sha2(ser(K)))

        # Prover finally creates an exponent that the verifier can use
        # to check that the prover really does have access to x
        # Note that if everything is done correctly, then the verifier
        # has to check g^s = X^challenge * K.
        # Given X, K, and s, the prover still does not have enough information
        # to compute x
        s = Fp(k + c * x)

        # The proof consists of K and s.
        # This is enough information for the verifier to validate that the
        # client knows x.
        return (K,s)
