"""
Client sends requests to make accounts and processes authentication requests

Client serves as a wrapper around Prover. While Prover deals with objects from the cryptographic libraries, Client conveniently takes all inputs as strings and passes all outputs as strings.

"""

__author__ = 'Abhiram Kothapalli'

from password import Password
from prover import Prover
from cryptography.secp256k1 import ser, deser
from cryptography.utilities import proof_to_str
from utilities.logger import Logger


class Client():

    logger = Logger()
    pwd = Password()
    prover = Prover()

    # Can be expanded in future weeks
    def __init__(self):
        return


    def register(self, password):
        """register registers a commitment with the server

        In particular, register takes a string argument and generates a commitment from it using the Prover class. Client converts this commitment into a string and returns it.

        Args:
        password: The users password. This is not stored anywhere.

        Returns:
        A commitment in the form of a string.

        """

        # Create a commitment with the password.
        commitment = ser(self.pwd.make_commitment(password))

        self.log("Sending registration request " + str(commitment))

        # Next week instead of returning this will send data to server.
        return commitment

    def authenticate(self,password):
        """authenticate sends an authentication request to the server

        In particular authenticate takes a password, converts it to an exponent, generates a proof from this exponent and returns the result.

        Returns:
        A proof in the form of a string.

        """

        # Create a proof with the password.
        exponent = self.pwd.password_to_exponent(password)

        proof = self.prover.prove(exponent)

        self.log("Sending authentication request " + str(proof_to_str(proof)))
        
        # Next week instead of returning this will send data to the server.
        return proof_to_str(proof)


    # Stubbed out for now. Next week this will be used for networking.
    def send(self, message):
        pass

    # Allows client to log using the Logger class.
    def log(self, msg):
        self.logger.log("Client", msg)
