""" TestProtocol tests all aspects of the zkpass library

In particular TestProtocol checks the prover, verifier, server, client, and bloomfilter.
The cryptographic libraries used are assumed to be secure.
"""

__author__ = 'Abhiram Kothapalli'

import unittest
import os
import sys
sys.path += ['../']

from zkpass.prover import Prover
from zkpass.verifier import Verifier
from zkpass.client import Client
from zkpass.server import Server
from zkpass.cryptography.secp256k1 import uint256_from_str, G, Fq, Point, curve, ser, deser
from zkpass.cryptography.utilities import rnd_bytes, H, str_to_proof, proof_to_str
from zkpass.password import Password
from zkpass.utilities.bloomfilter import BloomFilter


class TestProtocol(unittest.TestCase):


    def test_protocol(self):
        """test_protocol checks the prover and verifier

        Specifically, test_protocol creates a commitment from
        a given password and passes it to the verifier

        """

        password = "qwerty"

        pwd = Password()        

        commitment = pwd.make_commitment(password)
        x = pwd.password_to_exponent(password)

        
        p = Prover()
        v = Verifier()
        
        proof = p.prove(x)
        
        assert v.verify(commitment, proof)

    def test_client_networkless(self):
        """test_client_networkless checks the client in a networkless setting.

        Checks that client produces proofs that are accepted by the verifier.
        Also checks that the verifier does not accept invalid proofs.

        """

        # Generate a password
        password = "passw0rd"

        # Create a client and commitment.
        c = Client()
        commitment = deser(c.register(password))
        K, s = str_to_proof(c.authenticate(password))

        # Create a verifier to test against.
        v = Verifier()

        # Assert that clients commitment is valid.
        assert v.verify(commitment, (K, s))


        # Create a invalid password to authenticate previous commitment
        password = "passwerd"

        K, s = str_to_proof(c.authenticate(password))


        # Assert invalid password fails.
        assert not v.verify(commitment, (K, s))


    def test_server_networkless(self):
        """test_server_networkless checks the server in a networkless setting.

        Checks that the server accepts valid proofs. Uses the prover to test against.
        
        """

        # Create a password
        password = "123456"

        pwd = Password()        

        # Create a commitment
        commitment = pwd.make_commitment(password)
        x = pwd.password_to_exponent(password)

        # User prover to build a valid proof
        p = Prover()
        
        proof = p.prove(x)
  
        s = Server()

        # Register the commitment with the server
        s.register(ser(commitment))

        # Assert that a valid proof is accepted.
        assert s.authenticate(ser(commitment), proof_to_str(proof))


    def test_client_server(self):
        """test_client_server checks interacting client and server.

        Checks that a valid proof produced by the client is accepted by the server.
        Checks that an invalid proof produced by the client is rejected by the server.

        """

        # Create password.
        password = "passw0rd"

        # Initalize client and server.
        c = Client()
        s = Server()

        # Create commitment. 
        commitment = c.register(password)

        # Register the commitment with the server first.
        s.register(commitment)

        # Client now creates a proof.
        proof = c.authenticate(password)

        # Make sure server correctly accepts valid proof
        assert s.authenticate(commitment, proof)


        # Create password.
        password = "hello world"

        # Create a commitment.
        commitment = c.register(password)

        # Client now creates a proof.
        proof = c.authenticate(password)

        # Attempt to authenticate without first registering.
        assert not s.authenticate(commitment, proof)


    def test_bloom_filter(self):
        """test_bloom_filter checks the functionality of the implemented bloom filter

        """

        bloom = BloomFilter(50000, 7)

        # Add a string.
        bloom.add("apple")

        # Check that it can add the same string.
        bloom.add("apple")

        # Add more strings.
        bloom.add("banana")
        bloom.add("orange")

        # Check that bloom contains all added strings.
        assert "apple" in bloom
        assert "orange" in bloom
        assert "banana" in bloom

        # Check that plum is not in bloom filter.
        assert "plum" not in bloom


if __name__ == '__main__':
    unittest.main()

    

