Authentication protocol using Non-Interactive Schnorr commitments


pip dependencies:

bitarray
mmh3

Overview of Schnorr commitment protocol:

The client generates a random number k from a large sample space.
The client computes K = g^k and sends K to the server (k is kept secret)
The server generates a random challenge c from a large sample space.
The client calculates s = k + cx and sends s to the server
The server checks that g^s = X^cK. If the check holds then the client, in fact, knows x

Files I have written:

client.py
server.py
prover.py
verifier.py
password.py
cryptography/utilities.py
utilities/bloomfilter.py
utilities/logger.py
